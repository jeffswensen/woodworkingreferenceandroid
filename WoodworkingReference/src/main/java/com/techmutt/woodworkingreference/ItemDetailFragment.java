package com.techmutt.woodworkingreference;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private WRContent.WRItem mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = WRContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = null;
        if (mItem != null) {
            Activity act = getActivity();
            act.setTitle(mItem.title);
            rootView = inflater.inflate(mItem.layout, container, false);
            if(mItem.type == WRContent.WRItemType.JANKA) {
                TableLayout tableLayout = ((TableLayout) rootView.findViewById(R.id.jankaTableLayout));
                int rowNumber = 0;
                for (ArrayList<String> row : ((WRContent.JankaItem) mItem).values) {
                    TableRow tableRow = new TableRow(getActivity());
                    tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                    for (String cell : row) {
                        TextView cellView = new TextView(getActivity());
                        cellView.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1));
                        cellView.setText(cell);
                        cellView.setGravity(Gravity.CENTER);
                        tableRow.addView(cellView);
                    }
                    if (rowNumber % 2 == 0) {
                        tableRow.setBackgroundResource(R.drawable.rowColor1);
                    } else {
                        tableRow.setBackgroundResource(R.drawable.rowColor2);
                    }
                    tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    rowNumber++;
                }

            } else if(mItem.type == WRContent.WRItemType.SEGMENTEDMITRES) {
                TableLayout tableLayout = ((TableLayout) rootView.findViewById(R.id.segmentedMitresTableLayout));
                int rowNumber = 0;
                for (ArrayList<String> row : ((WRContent.SegmentedMitres) mItem).values) {
                    TableRow tableRow = new TableRow(getActivity());
                    tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                    for (String cell : row) {
                        TextView cellView = new TextView(getActivity());
                        cellView.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1));
                        cellView.setText(cell);
                        cellView.setGravity(Gravity.CENTER);
                        tableRow.addView(cellView);
                    }
                    if (rowNumber % 2 == 0) {
                        tableRow.setBackgroundResource(R.drawable.rowColor1);
                    } else {
                        tableRow.setBackgroundResource(R.drawable.rowColor2);
                    }
                    tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    rowNumber++;
                }
            } else if(mItem.type == WRContent.WRItemType.PILOTHOLES) {
                TableLayout tableLayout = ((TableLayout) rootView.findViewById(R.id.pilotHolesTableLayout));
                int rowNumber = 0;
                for (ArrayList<String> row : ((WRContent.ScrewPilotHoles) mItem).values) {
                    TableRow tableRow = new TableRow(getActivity());
                    tableRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    for (String cell : row) {
                        TextView cellView = new TextView(getActivity());
                        cellView.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1));
                        cellView.setText(cell);
                        cellView.setGravity(Gravity.CENTER);
                        tableRow.addView(cellView);
                    }
                    if (rowNumber % 2 == 0) {
                        tableRow.setBackgroundResource(R.drawable.rowColor1);
                    } else {
                        tableRow.setBackgroundResource(R.drawable.rowColor2);
                    }
                    tableLayout.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    rowNumber++;
                }
            } else if (mItem.type == WRContent.WRItemType.GOLDENRATIO) {
               // nothing to do
            }
        }

        return rootView;
    }

}
