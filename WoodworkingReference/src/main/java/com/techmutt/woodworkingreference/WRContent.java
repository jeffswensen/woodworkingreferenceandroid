package com.techmutt.woodworkingreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jswensen on 11/18/13.
 */
public class WRContent {

    public static List<WRItem> ITEMS = new ArrayList<WRItem>();
    public static Map<String, WRItem> ITEM_MAP = new HashMap<String, WRItem>();

    static {
        // Add 3 sample items.
        addItem(new JankaItem("1", "Janka Hardness Scale"));
        addItem(new GoldenRatio("2", "Golden Ratio Calculator"));
        addItem(new SegmentedMitres("3", "Segmented Circle Mitre Angles"));
        addItem(new ScrewPilotHoles("4", "Wood Screw Pilot Hole Sizes"));
    }

    private static void addItem(WRItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static class WRItem {
        public String id;
        public String title;
        public WRItemType type;
        public int layout;

        public WRItem(String id, String title, WRItemType type, int layout) {
            super();
            this.id = id;
            this.title = title;
            this.type = type;
            this.layout = layout;
        }

        @Override
        public String toString() {
            return title;
        }
    }

   /* public static class WRTableItem {
        public ArrayList<String> headers = new ArrayList<String>();
        public ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

        public WRTableItem(String id, String title) {
            super(id, title, WRItemType.TABLE, R.layout.generic_table);
        }
    }*/

    public static class JankaItem extends WRItem {

        public ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

        public JankaItem(String id, String title) {
            super(id, title, WRItemType.JANKA, R.layout.janka_table);
            this.addValue("Ebony", "3220");
            this.addValue("Brazilian Koa", "2160");
            this.addValue("Brazilian Walnut / Ipe", "3684");
            this.addValue("Bamboo", "3000");
            this.addValue("Red Mahogany", "2697");
            this.addValue("Brazilian Cherry", "2350");
            this.addValue("Mesquite", "2345");
            this.addValue("Bubinga", "1980");
            this.addValue("Purpleheart", "1860");
            this.addValue("Rosewood", "1780");
            this.addValue("Black Locust", "1700");
            this.addValue("Wenge", "1630");
            this.addValue("Zebrawood", "1575");
            this.addValue("White Oak", "1360");
            this.addValue("Ash", "1320");
            this.addValue("Red Oak", "1290");
            this.addValue("Teak", "1155");
            this.addValue("Cocobolo", "1136");
            this.addValue("Black Walnut", "1010");
            this.addValue("Cherry", "950");
            this.addValue("Red Maple", "950");
            this.addValue("Paper Birch", "910");
            this.addValue("Eastern Red Cedar", "900");
            this.addValue("Southern Yellow Pine", "870");
            this.addValue("Sycamore", "770");
            this.addValue("Douglas Fir", "660");
            this.addValue("Alder", "590");
            this.addValue("Chestnut", "540");
            this.addValue("Hemlock", "500");
            this.addValue("Basswood", "410");
            this.addValue("Balsa", "100");
        }

        private void addValue(String name, String hardness) {
            ArrayList<String> thisValue = new ArrayList<String>();
            thisValue.add(name);
            thisValue.add(hardness);
            this.values.add(thisValue);
        }
    }

    public static class SegmentedMitres extends WRItem {
        public ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

        public SegmentedMitres(String id, String title) {
            super(id, title, WRItemType.SEGMENTEDMITRES, R.layout.segmented_mitres);
            this.addValue("4", "45");
            this.addValue("5", "36");
            this.addValue("6", "30");
            this.addValue("8", "22.5");
            this.addValue("9", "20");
            this.addValue("10", "18");
            this.addValue("12", "15");
            this.addValue("15", "12");
            this.addValue("18", "10");
            this.addValue("20", "9");
        }

        private void addValue(String numberOfSegments, String mitreAngle) {
            ArrayList<String> thisValue = new ArrayList<String>();
            thisValue.add(numberOfSegments);
            thisValue.add(mitreAngle);
            this.values.add(thisValue);
        }
    }

    public static class ScrewPilotHoles extends WRItem {
        public ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();

        public ScrewPilotHoles(String id, String title) {
            super(id, title, WRItemType.PILOTHOLES, R.layout.pilot_hole_sizes);
            this.addValue("#2", "1/16", "1/16");
            this.addValue("#3", "1/16", "1/16");
            this.addValue("#4", "5/64", "1/16");
            this.addValue("#5", "5/64", "1/16");
            this.addValue("#6", "3/32", "5/64");
            this.addValue("#7", "7/64", "3/32");
            this.addValue("#8", "7/64", "3/32");
            this.addValue("#9", "1/8", "7/64");
            this.addValue("#10", "1/8", "7/64");
            this.addValue("#12", "9/64", "1/8");
            this.addValue("#14", "5/32", "9/64");
        }

        private void addValue(String screwGauge, String hardwoodSize, String softwoodSize) {
            ArrayList<String> thisValue = new ArrayList<String>();
            thisValue.add(screwGauge);
            thisValue.add(hardwoodSize);
            thisValue.add(softwoodSize);
            this.values.add(thisValue);
        }
    }

    public static class GoldenRatio extends WRItem {
        public GoldenRatio(String id, String title) {
            super(id, title, WRItemType.GOLDENRATIO, R.layout.golden_ratio);
        }

    }

    public static enum WRItemType {
         GOLDENRATIO, PILOTHOLES, SEGMENTEDMITRES, JANKA
    }
}
